import { me } from 'companion';
import * as messaging from 'messaging';
import { settingsStorage } from 'settings';
import { geolocation } from 'geolocation';

const EMPTY = "";
const PLUS = '+';
const MINUS = '-';
const SEP = ':';
const ZERO = '0';
const HOUR = 60;
const MINUTE = 60;

function hhmm(iso) {
  return new Date(iso).toTimeString().substr(0,5);
}

function approx(coord) {
  let PRECISION = 10;
  return Math.round(coord * PRECISION) / PRECISION;
}

var cfg = {
	'fin': {
		'endpoint' : 'https://api.rolandvanipenburg.com/v1/fin.json',
		'command': 'fin',
		'payload': json => {
      return json;
    },
    'cap' : MINUTE,
    'previous' : 0
  },
	'space': {
		'endpoint' : 'https://api.rolandvanipenburg.com/v1/people_in_space.json',
		'command': 'space',
		'payload': json => {
      return {space: json['number']};
    },
    'cap' : MINUTE,
    'previous' : 0
  },
	'astro': {
		'endpoint' : (lat, lon, stamp) => {
      stamp = stamp.replace(/[.]\d{3}/, EMPTY);
      return `https://api.rolandvanipenburg.com/v1/astro/event.json?eventVal1=${lat}&eventVal2=${lon}&eventVal3=0&eventStart=${stamp}&eventSearch=1&event1Id=600&event2Id=610&event3Id=800&event4Id=810&event5Id=110`;
    },
    'geo' : 1,
		'command' : 'astro',
		'payload' : json => {
      let sun = [];
      let moon = [];
      let moonphase;
      json.astrodata.Event.forEach(event => {
        event.Report.forEach(rep => {
          let id = rep['@repId'];
          if (id == '600' || id == '610') {
            sun.push(rep['@time']);
          }
          else if (id == '800' || id == '810') {
            moon.push(rep['@time']);
          }
          else if (id = '115') {
            moonphase = rep['@repVal'];
          }
        });
      });
      sun.sort();
      // Don't sort the moon, it's not as clear as with the sun what is what.
      return {
        next_sun_event: hhmm(sun[0]),
        after_next_sun_event: hhmm(sun[1]),
        next_moon_event: hhmm(moon[0]),
        after_next_moon_event: hhmm(moon[1]),
        moonphase: Math.round(moonphase)
      }
    },
    'cap' : MINUTE,
    'previous' : 0
  }
};

function call_api(lbl, endpoint) {
  if (me.permissions.granted('access_internet') ) {
    fetch(endpoint)
    .then( res => {
      if (!res.ok) {
        throw new Error(`"${res.url}" request got ${res.status}`);
      }
      return res;
    })
    .then( res => res.json() )
    .then( json => deliver( cfg[lbl]['payload'](json) ) )
    .catch( err => console.error(err) );
  }
  else {
    console.log('No internet access granted to call API');
  }
}

function query_service(lbl) {
  let now = Date.now() / 1000;
  if (now - cfg[lbl]['cap'] > cfg[lbl]['previous']) {
    cfg[lbl]['previous'] = now;
    if (cfg[lbl]['geo']) {
      if (me.permissions.granted('access_location') ) {
        geolocation.getCurrentPosition(position => {
          let endpoint = cfg[lbl]['endpoint'](
            approx(position.coords.latitude),
            approx(position.coords.longitude),
            encodeURIComponent(new Date(now * 1000).toISOString() )
          );
          call_api(lbl, endpoint);
        });
      }
      else {
        console.log('No location permission granted to get geolocation');
      }
    }
    else {
      call_api(lbl, cfg[lbl]['endpoint']);
    }
	}
}

function deliver(data) {
  if (messaging.peerSocket.readyState === messaging.peerSocket.OPEN) {
    messaging.peerSocket.send(data);
  }
}

messaging.peerSocket.addEventListener('message', (evt) => {
	let MAP = {
		'fin': 'fin',
		'space': 'space',
		'astro': 'astro',
	};
  if (evt.data && evt.data.command in MAP) {
    query_service(MAP[evt.data.command]);
  }
});

settingsStorage.addEventListener('change', evt => {
  if (evt.oldValue !== evt.newValue) {
    var val = JSON.parse(evt.newValue).values[0].value;
    if (val) {
      deliver({
        key: evt.key,
        value: val
      });
    }
  }
});
