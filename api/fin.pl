#!/usr/bin/env perl
# -*- cperl; cperl-indent-level: 4 -*-
# Copyright (C) 2021, Roland van Ipenburg
use strict;
use warnings;
use utf8;
use 5.020000;

BEGIN { our $VERSION = v0.0.1; }

use autodie qw(open close);
use Carp qw(croak);
use HTTP::Tiny::Cache;
use HTTP::Date qw(time2str str2time);
use JSON;
use English qw( -no_match_vars );

use Readonly;
## no critic (ProhibitCallsToUnexportedSubs)
Readonly::Scalar my $CACHE   => 60;
Readonly::Scalar my $KEY     => q{HTTP_TINY_CACHE_MAX_AGE};
Readonly::Scalar my $BITONIC => q{https://bitonic.nl/api/buy};
Readonly::Scalar my $AMSGOLD =>
  q{https://www.amsterdamgold.com/nl/rates/ajax/getRates/};
Readonly::Scalar my $MODIFIED     => 9;
Readonly::Scalar my $NOT_MODIFIED => 304;
Readonly::Hash my %LOG            =>
  ( 'page' => q{Could not retrieve one of the resources to get data: %s}, );
## use critic

my $out = shift @ARGV;
my $btc;
my $aux;
my $headers = {};
if ( defined $out && -r $out ) {
    open my $fh, '<:encoding(UTF-8)', $out;
    my $mod = HTTP::Date::time2str( ( stat $fh )[$MODIFIED] );
    local $INPUT_RECORD_SEPARATOR = undef;
    my $json  = JSON->new();
    my $store = $json->decode(<$fh>);
    close $fh;
    $btc = ${$store}{'btc'};
    $aux = ${$store}{'aux'};

    if ( defined $mod ) {
        $headers = {
            'headers' => {
                'If-Modified-Since' => $mod,
            },
        };
    }
}
## no critic (RequireLocalizedPunctuationVars)
$ENV{$KEY} = $ENV{$KEY} // $CACHE;
## use critic
my $success  = 0;
my $json     = JSON->new();
my $response = HTTP::Tiny::Cache->new->get( $BITONIC, $headers );
if ( ${$response}{'success'} ) {
    my $payload = $json->decode( ${$response}{'content'} );
    $btc = ${$payload}{'eur'};
    $success++;
}
elsif ( ${$response}{'status'} == $NOT_MODIFIED ) {
    $success++;
}
$response = HTTP::Tiny::Cache->new->get( $AMSGOLD, $headers );
if ( ${$response}{'success'} ) {
    my $payload = $json->decode( ${$response}{'content'} );
    $aux = ${$payload}{'gold'};
    $aux =~ s{[.]}{}gimsx;
    $success++;
}
elsif ( ${$response}{'status'} == $NOT_MODIFIED ) {
    $success++;
}

# Only update the result when both requests were successful to avoid updating
# the modification date using stale values:
if ( 2 == $success ) {
    my $fh;
    if ( defined $out ) {
        open $fh, '>:encoding(UTF-8)', $out;
    }
    else {
        $fh = *STDOUT;
    }
    printf {$fh} q{{"btc" : %s, "aux": %s}}, $btc, $aux;
    close $fh;
}
else {
    Carp::croak( sprintf $LOG{'page'}, $ERRNO );
}
