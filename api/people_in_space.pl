#!/usr/bin/env perl
# -*- cperl; cperl-indent-level: 4 -*-
# Copyright (C) 2021, Roland van Ipenburg
use strict;
use warnings;
use utf8;
use 5.020000;

BEGIN { our $VERSION = v0.0.1; }

use autodie qw(open close);
use Carp qw(croak);
use HTTP::Tiny::Cache;
use HTTP::Date qw(time2str str2time);
use JSON;
use English qw( -no_match_vars );

use Readonly;
## no critic (ProhibitCallsToUnexportedSubs)
Readonly::Scalar my $CACHE => 60;
Readonly::Scalar my $KEY   => q{HTTP_TINY_CACHE_MAX_AGE};
Readonly::Scalar my $URL   =>
  q{https://www.howmanypeopleareinspacerightnow.com/peopleinspace.json};
Readonly::Scalar my $MODIFIED     => 9;
Readonly::Scalar my $NOT_MODIFIED => 304;
Readonly::Hash my %LOG            => (
    'page' => q{Could not retrieve resource '%s' to get data: %s},
    'not'  => q{Resource '%s' wasn't modifed since %s},
);
## use critic

my $out     = shift @ARGV;
my $headers = {};
if ( defined $out && -r $out ) {
    open my $fh, '<:encoding(UTF-8)', $out;
    my $mod = HTTP::Date::time2str( ( stat $fh )[$MODIFIED] );
    close $fh;
    if ( defined $mod ) {
        $headers = {
            'headers' => {
                'If-Modified-Since' => $mod,
            },
        };
    }
}
## no critic (RequireLocalizedPunctuationVars)
$ENV{$KEY} = $ENV{$KEY} // $CACHE;
## use critic
my $json     = JSON->new();
my $response = HTTP::Tiny::Cache->new->get( $URL, $headers );
if ( ${$response}{'success'} ) {
    my $pay    = $json->decode( ${$response}{'content'} );
    my $number = ${$pay}{'number'};
    my $fh;
    if ( defined $out ) {
        open $fh, '>:encoding(UTF-8)', $out;
    }
    else {
        $fh = *STDOUT;
    }
    printf {$fh} q{{"number" : %d}}, $number;
    close $fh;
}
else {
    if ( ${$response}{'status'} == $NOT_MODIFIED ) {

        #Carp::croak( sprintf $LOG{'not'}, $URL, $mod );
    }
    else {
        Carp::croak( sprintf $LOG{'page'}, $URL, $ERRNO );
    }
}
