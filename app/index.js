import clock from 'clock';
import document from 'document';
import * as util from '../common/util';
import { me } from 'appbit';
import { today } from 'user-activity';
import { HeartRateSensor } from 'heart-rate';
import { BodyPresenceSensor } from 'body-presence';
import { display } from 'display';
import * as messaging from 'messaging';
import fs from 'fs';

const UNDEF = 'undefined';
const MAX_ARC = 30;
const MAX_RADIUS = 9999;

const LUT = [
  [ 0,   0,-30,-30,84,   0, 0, 0, 0],
  [ 1,-180,-28,-30,80,-180, 0,48, 0],
  [ 2,-180,-26,-30,76,-180, 0,48, 0],
  [ 3,-180,-24,-30,72,-180, 0,48, 0],
  [ 4,-180,-22,-30,69,-180, 0,48, 0],
  [ 5,-180,-20,-30,65,-180, 0,48, 0],
  [ 6,-180,-18,-30,61,-180, 0,48, 0],
  [ 7,-180,-18,-30,60,-180,11, 0, 3],
  [ 8,-180,-18,-30,60,-180, 9, 0, 7],
  [ 9,-180,-18,-30,60,-180, 7, 0,11],
  [10,-180,-18,-30,60,-180, 5, 0,14],
  [11,-180,-18,-30,60,-180, 3, 0,18],
  [12,-180,-18,-30,60,-180, 1, 0,22],
  [13, 180,-18,-30,60, 180, 1, 0,22],
  [14, 180,-18,-30,60, 180, 3, 0,18],
  [15, 180,-18,-30,60, 180, 5, 0,14],
  [16, 180,-18,-30,60, 180, 7, 0,11],
  [17, 180,-18,-30,60, 180, 9, 0, 7],
  [18, 180,-18,-30,60, 180,11, 0, 3],
  [19, 180,-18,-30,61, 180,12,48, 0],
  [20, 180,-20,-30,65, 180,12,48, 0],
  [21, 180,-22,-30,69, 180,12,48, 0],
  [22, 180,-24,-30,72, 180,12,48, 0],
  [23, 180,-26,-30,76, 180,12,48, 0],
  [24, 180,-28,-30,80, 180,12,48, 0]
];

const SETTINGS_TYPE = 'cbor';
const SETTINGS_FILE = 'settings.cbor';
const MAP = {
  'manic': 60,
  'cool': 300,
  'flex': 1800,
  'zero': Infinity
};

var PROP = {
  'btc'                   : undefined,
  'space'                 : undefined,
  'aux'                   : undefined,
  'next_sun_event'        : undefined,
  'after_next_sun_event'  : undefined,
  'next_moon_event'       : undefined,
  'after_next_moon_event' : undefined,
  'moonphase'             : undefined
};
let settings = load_settings();
apply_settings();

if ( me.permissions.granted( 'access_heart_rate' ) && HeartRateSensor ) {
  const hrs = new HeartRateSensor();
  hrs.start();
}

if ( me.permissions.granted( 'access_activity' ) && BodyPresenceSensor &&
  hrs ) {
  const bdy = new BodyPresenceSensor();
  bdy.addEventListener( 'reading', () => {
    if ( bdy.present ) {
      hrs.start();
    } else {
      hrs.stop();
    }
  } );
  bdy.start();
}

if ( display && hrs ) {
  display.addEventListener( 'change', () => {
    if ( hrs != null ) {
      if ( display.on ) {
        hrs.start();
      } else {
        hrs.stop();
      }
    }
  } );
}

if ( display ) {
  display.addEventListener( 'change', () => {
    if ( display.on ) {
      grab();
    }
  } );
}

const dow = document.getElementById( 'dow' );
const day = document.getElementById( 'day' );
const month = document.getElementById( 'month' );
const hours = document.getElementById( 'hours' );
const sep = document.getElementById( 'sep' );
const minutes = document.getElementById( 'minutes' );
const seconds = document.getElementById( 'seconds' );
const steps = document.getElementById( 'steps' );
const heartrate = document.getElementById( 'heartrate' );
const pic = document.getElementById( 'moonphase_pic' );
const gibbous = document.getElementById( 'gibbous' );
const crescent = document.getElementById( 'crescent' );

let previous_phase;

clock.granularity = "seconds";
clock.ontick = ( evt ) => {
  let now = evt.date;
  seconds.text = util.monoDigits( now.getSeconds() );
  sep.text = ':';
  minutes.text = util.monoDigits( now.getMinutes() );
  hours.text = util.monoDigits( now.getHours() );
  dow.text = util.dow_abbr( now.getDay() );
  day.text = util.monoDigits( now.getDate(), false );
  month.text = util.month_abbr( now.getMonth() );
  if (me.permissions.granted('access_activity') && !isNaN(today.adjusted.steps) ) {
    steps.text = util.monoDigits( util.moon_fmt( today.adjusted.steps, 4 ),
    false );
  }
  if (me.permissions.granted('access_heart_rate') && hrs ) {
    heartrate.text = ( hrs.heartRate == null ) ?
      '--' :
      util.monoDigits( hrs.heartRate, false );
  }

  let phase = PROP.moonphase;
  if (me.permissions.granted('access_internet') ) {
    if (typeof phase !== UNDEF) {
      pic.style.display = 'block';
      phase /= 100;
      let lut = LUT[Math.floor(phase * 25)];
      if (phase !== previous_phase) {
        gibbous.sweepAngle = lut[1];
        crescent.sweepAngle = lut[5];
        gibbous.height = (2 * MAX_ARC) + pic.width;
        gibbous.arcWidth = MAX_ARC;
        if (phase > 0 && phase < .25) {
          gibbous.sweepAngle = lut[1];
          gibbous.x = lut[2];
          gibbous.y = lut[3];
          gibbous.width = lut[4];
          crescent.sweepAngle = lut[5];
          crescent.y = lut[7];
        }
        else if (phase >= .25 && phase < .5) {
          gibbous.sweepAngle = lut[1];
          gibbous.width = lut[4];
          gibbous.x = lut[2];
          gibbous.y = lut[3];
          crescent.sweepAngle = lut[5];
          crescent.width = lut[8];
          crescent.x = lut[6];
          crescent.y = lut[7];
        }
        else if (phase >= .5 && phase < .75) {
          gibbous.sweepAngle = lut[1];
          gibbous.x = lut[2];
          gibbous.y = lut[3];
          gibbous.width = lut[4];
          crescent.sweepAngle = lut[5];
          crescent.x = lut[6];
          crescent.y = lut[7];
          crescent.width = lut[8];
        }
        else if (phase >= .75) {
          gibbous.sweepAngle = lut[1];
          gibbous.x = lut[2];
          gibbous.y = lut[3];
          gibbous.width = lut[4];
          crescent.sweepAngle = lut[5];
          crescent.x = lut[6];
          crescent.y = lut[7];
          crescent.width = lut[8];
        }
        previous_phase = phase;
      }
    }
  }
  else {
    pic.style.display = 'none';
    console.log('No internet access granted to get moon phase data');
  }
}

function fetch_cmd( cmd ) {
  if (me.permissions.granted('access_internet') ) {
    if ( messaging.peerSocket.readyState === messaging.peerSocket.OPEN ) {
      messaging.peerSocket.send( {
        command: cmd
      } );
    }
  }
  else {
    console.log('No internet access granted to get data');
  }
}

function grab() {
  fetch_cmd( 'fin' );
  fetch_cmd( 'astro' );
  fetch_cmd( 'space' );
}

function process( dta ) {
  for (var p in PROP) {
    if ( p in dta && typeof dta[ p ] !== UNDEF ) {
      let el = document.getElementById( p );
      if (el !== null) {
        el.text = util.monoDigits(dta[ p ], false );
      }
      PROP[p] = dta[p];
    }
  }
  if ( dta.key == 'delay' ) {
    settings.delay = dta.value;
    apply_settings();
    save_settings();
  }
}

messaging.peerSocket.addEventListener( 'open', () => {
  grab();
} );

messaging.peerSocket.addEventListener( 'message', ( evt ) => {
  if ( evt.data ) {
    process( evt.data );
  }
} );

var api_call;

function apply_settings() {
  if ( settings.delay in MAP ) {
    let delay = MAP[ settings.delay ];
    if ( api_call ) {
      clearInterval( api_call );
    }
    if ( delay < Infinity ) {
      api_call = setInterval( grab, delay * 1000 );
    }
  }
}

function load_settings() {
  let json;
  try {
    json = fs.readFileSync( SETTINGS_FILE, SETTINGS_TYPE );
  } catch ( ex ) {
    json = {
      delay: 'cool'
    }
  }
  return json;
}

function save_settings() {
  try {
    fs.writeFileSync( SETTINGS_FILE, settings, SETTINGS_TYPE );
  } catch (ex) {
    console.log("Couldn't save settings", ex);
  }
}

me.onunload = save_settings;
