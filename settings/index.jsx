registerSettingsPage(({ settings }) => (
  <Page>
    <Section
      title={
        <Text bold align="center">
          Clock Face Settings
        </Text>
      }
    >
      <Select
        label={`Connect to the internet to update the data`}
        settingsKey='delay'
        options={[
          {name:"every minute", value: 'manic' },
          {name:"every five minutes", value: 'cool' },
          {name:"every half hour", value: 'flex' },
          {name:"never", value: 'zero' }
        ]}
      />
    </Section>
    <Section
      title={
        <Text bold align="center">
          Source code and data sources
        </Text>
      }
    >
      <Link source="https://bitbucket.org/rolandvanipenburg/fitbit-clock-face">Bitbucket</Link>
      <Link source="https://bitonic.nl">Bitonic.nl</Link>
      <Link source="https://amsterdamgold.com">AmsterdamGold.com</Link>
      <Link source="https://github.com/FrankThomasTveter/astro-api">The Norwegian Meteorological Institute</Link>
      <Link source="https://www.howmanypeopleareinspacerightnow.com">How Many People Are in Space Right Now?</Link>
    </Section>
  </Page>
));
